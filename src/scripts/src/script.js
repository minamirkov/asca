/* eslint-disable no-console */
document.addEventListener('DOMContentLoaded', function() {
    // - Code to execute when all DOM content is loaded
});
const menu = document.querySelector('.menu');
const menuItems = document.querySelectorAll('.menuItem');
const hamburger = document.querySelector('.hamburger');
const closeIcon = document.querySelector('.closeIcon');
const menuIcon = document.querySelector('.menuIcon');

function toggleMenu() {
    if (menu.classList.contains('showMenu')) {
        menu.classList.remove('showMenu');
        closeIcon.style.display = 'none';
        menuIcon.style.display = 'block';
    } else {
        menu.classList.add('showMenu');
        closeIcon.style.display = 'block';
        menuIcon.style.display = 'none';
    }
}

hamburger.addEventListener('click', toggleMenu);

menuItems.forEach(function (menuItem) {
    menuItem.addEventListener('click', toggleMenu);
});
const fraction = document.getElementById('fraction');
const fraction1 = document.getElementById('fraction1');
const slides = document.querySelectorAll('.m-hero');
const slideCount = slides.length;
fraction.textContent = `01 / 0${slideCount}`;
fraction1.textContent = `00 | 03`;



const swiper = new Swiper('.mySwiper', {
    keyboard: {
        enabled: true,
    },
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true,
    },

    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    on: {
        slideChange: () => {
            fraction.textContent = ` 0${swiper.realIndex + 1} / 0${slideCount}`;
            fraction1.textContent = ` 0${swiper.realIndex} | 0${Math.abs(swiper.realIndex - 3)}`;
        }
    }
});
const meni = ['TREES', 'MOUNTAINS', 'JAPAN', 'SEA LEVELS', 'TREES', 'MOUNTAINS' ];
const swiperGallery = new Swiper('.gallerySwiper', {
    slidesPerView: 4,
    spaceBetween: 32,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
        renderBullet: function (index, className) {
            return '<span class="' + className + '">' + (meni[index]) + '</span>';
        },
    },
    breakpoints: {
        480: {
            slidesPerView: 1,
            spaceBetween: 15,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        992: {
            slidesPerView: 3,
            spaceBetween: 32,
        },
        1344: {
            slidesPerView: 4,
            spaceBetween: 32,
        },
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});
