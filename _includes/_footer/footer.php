<footer>
    <div class="m-footer" id="contact">
        <div class="_wr">
            <div class="_w">
                <div class="_xs12 _s12 _m6 _l3 _3">
                    <p class="m-footer__headings"> ABOUT US </p>
                    <p class="pink"> Reaching Our Goals </p>
                    <p class="m-footer__paragraph"> Purpose of the organization is to contribute to sound education of children, to protect human rights, and to promote truly ever-lasting world peace </p>
                    <button> Read more
                        <hr> </button>
                </div>
                <div class="_xs12 _s12 _m6 _l3 _3">
                    <p class="m-footer__headings"> EVENTS </p>
                    <div class="column">
                        <div>
                            <p> Sli Lanka </p>
                            <p> Tibet </p>
                            <p> Lao PDR </p>
                            <p> Nepal </p>
                        </div>
                        <div>
                            <p> Sli Lanka </p>
                            <p> Tibet </p>
                            <p> Lao PDR </p>
                        </div>
                    </div>
                </div>
                <div class="_xs12 _s12 _m6 _l3 _3">
                    <p class="m-footer__headings" > LATEST NEWS </p>
                    <img src="src/images/_images/The_Common_Wanderer_best_things_to_do_Nepal-50.jpg" alt="humans" class="img">
                    <p class="pink"> We are going to Nepal!</p>
                    <button> Read more
                        <hr> </button>
                </div>
                <div class="_xs12 _s12 _m6 _l3 _3">
                    <p class="m-footer__headings"> CONTACT </p>
                    <p> Phone &emsp;<span>/</span>&emsp; +381 64 997 33 77     </p>
                    <p> Email &emsp;<span>/</span>&emsp; placeholder@office.rs</p>
                    <p> Adress &emsp;<span>/</span>&emsp; placeholder, Novi Sad      </p>

                    <p class="m-footer__headings social"> SOCIAL MEDIA </p>
                    <div class="icons">
                        <a href="#"> <img src="src/images/_icons/facebook-logo-button@2x.png" alt="facebook"> </a>
                        <a href="#"> <img src="src/images/_icons/twitter-logo-button@2x.png" alt="twitter"> </a>
                        <a href="#"> <img src="src/images/_icons/linkedin-logo-button@2x.png" alt="linkedin"> </a>
                        <a href="#"> <img src="src/images/_icons/instagram-logo@2x.png" alt="instagram"> </a> <br>
                        <a href="#"> <img src="src/images/_icons/pinterest-logo-button@2x.png" alt="pinterest"> </a>
                        <a href="#"> <img src="src/images/_icons/Slack@2x.png" alt="slack"> </a>
                        <a href="#"> <img src="src/images/_icons/Youtube@2x.png" alt="youtube"> </a>
                    </div>
                </div>
                <div class="_wr">
                    <p class="m-footer__headings"> NAVIGATION </p>
                    <div class="m-footer__nav">
                        <button onclick="window.location.href='#'"> About Us <hr> </button>
                        <button onclick="window.location.href='#history'"> A Wealth of History <hr> </button>
                        <button onclick="window.location.href='#past-activities'"> Past activities <hr></button>
                        <button onclick="window.location.href='#news'"> News <hr> </button>
                        <button onclick="window.location.href='#contact'"> Contact <hr></button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</footer>

<?php require_once(dirname(__FILE__) . '/footer_meta.php'); ?>