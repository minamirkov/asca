<!--dati nazive sekcijama tako da se mogu koristiti na razlicitim mestima, ukoliko se pojave iste sekcije
    ovako mozda neces moci da se setis sta ti je section1 npr. ovo bi moglo da bude m-slider ili m-hero -->
<div class="swiper mySwiper">
    <div class="swiper-wrapper">
        <div class="swiper-slide">
            <div class="m-hero" id="about-us">
                <div class="m-hero__left">
                    <p class="m-hero__left--p2"> Learning more about ASCA </p>
                    <h1> Reaching Our Goals</h1>
                    <p class="m-hero__left--p3">
                        Purpose of the organization is to contribute to sound education of
                        children, to protect human rights, and to promote truly ever-lasting
                        world peace </p>
                    <button onclick="window.location.href='#'"> Learn more </button>
                </div>
                <div class="m-hero__right">
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="m-hero" id="about-us">
                <div class="m-hero__left">
                    <!-- <p class="m-hero__left--p1"> 02 <span> / 04 </span> </p> -->
                    <p class="m-hero__left--p2"> Learning more about ASCA </p>
                    <h1> Reaching Our Goals</h1>
                    <p class="m-hero__left--p3">
                        Purpose of the organization is to contribute to sound education of
                        children, to protect human rights, and to promote truly ever-lasting
                        world peace </p>
                    <button onclick="window.location.href='#'"> Learn more </button>
                </div>
                <div class="m-hero__right">
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="m-hero" id="about-us">
                <div class="m-hero__left">
                    <!-- <p class="m-hero__left--p1"> 02 <span> / 04 </span> </p> -->
                    <p class="m-hero__left--p2"> Learning more about ASCA </p>
                    <h1> Reaching Our Goals</h1>
                    <p class="m-hero__left--p3">
                        Purpose of the organization is to contribute to sound education of
                        children, to protect human rights, and to promote truly ever-lasting
                        world peace </p>
                    <button onclick="window.location.href='#'"> Learn more </button>
                </div>
                <div class="m-hero__right">
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="m-hero" id="about-us">
                <div class="m-hero__left">
                    <!-- <p class="m-hero__left--p1"> 02 <span> / 04 </span> </p> -->
                    <p class="m-hero__left--p2"> Learning more about ASCA </p>
                    <h1> Reaching Our Goals</h1>
                    <p class="m-hero__left--p3">
                        Purpose of the organization is to contribute to sound education of
                        children, to protect human rights, and to promote truly ever-lasting
                        world peace </p>
                    <button onclick="window.location.href='#'"> Learn more </button>
                </div>
                <div class="m-hero__right">
                </div>
            </div>
        </div>
    </div>
    <div id="fraction"> </div>
    <div class="swiper-pagination"></div>
    <div class="swiper-button-prev"> &larr; </div>
    <div id="fraction1"> </div>
    <div class="swiper-button-next"> &rarr; </div>
</div>

<?php require_once(dirname(__FILE__) . '/index.php'); ?>