<!--isto kao i u prethodnom fajlu, probaj da nazoves tako sekcije da znas koju celinu obuhvataju-->
<div class="m-gallery" id="history">
    <div class="m-gallery__top">
        <p class="m-gallery__top--apart"> BE APART OF A </p>
        <h2> Wealth of history </h2>
        <p class="m-gallery__top--tibet"> From Tibet to Nepal, from Sli Lanka to Lao we have touched
            many souls and have made history with events and movements. </p>
    </div>
    <div class="swiper gallerySwiper">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <img src="src/images/_images/unsplash.jpg" alt="">
                <button onclick="window.location.href='#'">  SLI LANKA <br> <span> (8) Events </span> </button>
            </div>
            <div class="swiper-slide">
                <img src="src/images/_images/arisu-ling-ikaPAfq6Z_I-unsplash.jpg" alt="">
                <button onclick="window.location.href='#'">  TIBET <br> <span> (3) Events </span></button>
            </div>
            <div class="swiper-slide">
                <img src="src/images/_images/rohit-tandon-9wg5jCEPBsw-unsplash.jpg" alt="">
                <button onclick="window.location.href='#'">  NEPAL <br> <span> (12) Events </span> </button>
            </div>
            <div class="swiper-slide">
                <img src="src/images/_images/ioana-farcas-BMXTDz-wZT4-unsplash.jpg" alt="">
                <button onclick="window.location.href='#'"> LAO PDR <br> <span> (9) Events </span> </button>
            </div>
            <div class="swiper-slide">
                <img src="src/images/_images/unsplash.jpg" alt="">
                <button onclick="window.location.href='#'">  SLI LANKA <br> <span> (8) Events </span> </button>
            </div>
            <div class="swiper-slide">
                <img src="src/images/_images/arisu-ling-ikaPAfq6Z_I-unsplash.jpg" alt="">
                <button onclick="window.location.href='#'">  TIBET <br> <span> (3) Events </span></button>
            </div>
        </div>
        <div class="swiper-button-prev"> &larr; </div>
        <div class="swiper-button-next"> &rarr; </div>
        <div class="swiper-pagination"></div>
    </div>
</div>

<?php require_once(dirname(__FILE__) . '/index.php'); ?>