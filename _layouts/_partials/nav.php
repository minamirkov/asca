<!--samo predlog umesto __right, __left -> __search; __logo; __menu-->
<head> <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> </head>
<nav>
    <div class="m-nav">
        <div class="m-nav__logo">
            <img src="src/images/_icons/ASCA-logo%20Copy%203@2x.png" alt="logo">
        </div>
        <div class="m-nav__menu">
            <a href="#about-us"> About Us </a>
            <a href="#history"> A Wealth of History </a>
            <a href="#past-activities"> Past activities </a>
            <a href="#news"> News </a>
            <a href="#contact"> Contact </a>
            <a href="#"> <img src="" alt=""></a>
        </div>
        <div class="m-nav__search">
            <img src="src/images/_icons/search%20(2)@2x.png" alt="search">
            <a href=""> EN </a>
        </div>
    </div>
</nav>

<ul class="menu">
    <li><a class="menuItem" href="#">About Us</a></li>
    <li><a class="menuItem" href="#">A Wealth of History</a></li>
    <li><a class="menuItem" href="#">Past activities</a></li>
    <li><a class="menuItem" href="#">News</a></li>
    <li><a class="menuItem" href="#">Contact</a></li>
</ul>
<button class="hamburger">
    <i class="menuIcon material-icons">menu</i>
    <i class="closeIcon material-icons">close</i>
</button>